libname MetaDLib "S:\DataImports\MetadataLibrary";

/*proc sql;*/
/*	create table MetaDLib.fieldinformationMod as*/
/*	select * from MetaDLib.fieldinformation;*/
/*quit;*/
/**/
/*proc sql;*/
/*	update MetaDLib.fieldinformationMod*/
/*	set field = "PREVEM"*/
/*	where field = "PREVEMP";*/
/*quit;*/

%let fileID = 1;
%let return = C15078FE;

%macro fileload(return= ,fileID=, delimiter = "|");

proc sql; 
	reset noprint;
	select count(*) into :numfiles 
	from MetaDLib.infiles 
	where returnID="&return." and FileID=&fileid.;
quit;

/* Left join FieldInformation to FieldOrder */
proc sql;
 	create table inputMeta as 
	select	o.table, o.order,
			i.field, i.type, i.len as length, i.format, i.informat, i.label 
	from MetaDLib.Fieldorder(where=(returnID="&return." and FileID=&fileid.)) as o 
	left join MetaDLib.FieldInformationmod as i
 	on o.field = i.field and o.fieldversion=i.version 
	where inXML = 'Y'
	order by table, order;
quit;

/* Decide which tables need denormalising, based on upper case -  there's probably a better way of doing this? */
/* Count the number of tables that need denormalising as Numfiles */

proc sql;
	create table DoNotDenormalise as
	select * from inputMeta
	where table ^= upcase(table) and table ^= "DLHERecord";

	select count (distinct table) into :Numfiles 
	from DoNotDenormalise;

	create table tables as
	select distinct table 
	from DoNotDenormalise
quit;



%macro LoadTable(Table); 

/* Generate macro variables for statements */
proc sql;
	reset noprint;
	select catx(' ',field) into :FieldNames separated by ' ' from DoNotDenormalise
		where table = "&Table.";
	select 
		case	when type = 'Char' then	catx(' ',field,'$', length)
				else catx(' ',field, length)
		end 	into :lengthstatement separated by ' ' from DoNotDenormalise
		where table = "&Table.";
	select catx(' ',field,format) into :formatstatement separated by ' ' from DoNotDenormalise
		where table = "&Table.";
	select catx(' ',field,informat) into :informatstatement separated by ' ' from DoNotDenormalise
		where table = "&Table.";
	select catx(' ',field,'=',label) into :labelstatement separated by ' ' from DoNotDenormalise
		where table = "&Table.";
quit;

/*%put &FieldNames.;*/
/*%put &lengthstatement.;*/
/*%put &formatstatement.;*/
/*%put &informatstatement.;*/
/*%put &labelstatement.;*/

data &Table.;
	length recordID 8 parentID $ 5 &lengthstatement.;
	format recordID F8. parentID $5. &formatstatement.;
	informat recordID F8. parentID $5. &informatstatement.;
	label recordID = recordID parentID=parentID &labelstatement.;
	infile "S:\DataImports\SASCodeLibrary\Test_AS\Import\&Table..txt" encoding="utf-8" dsd delimiter='|' lrecl = 25000 pad missover firstobs = 2;
	input recordID parentID &FieldNames.;
run;

%mend;

/* Macroise this */

%LoadTable(Employment);
%LoadTable(Institution);
%LoadTable(Student);
%LoadTable(Study);
%LoadTable(Teaching);












/*proc sql;*/
/*	reset noprint;*/
/*	select count (distinct table) into :Numfiles */
/*	from Doesnotneeddenormalising;*/
/*	*/
/*	create table tables as*/
/*	select distinct table */
/*	from Doesnotneeddenormalising;*/
/*quit;*/
/**/
/*%put There are &Numfiles. tables to load;*/
/**/
/*%macro ActualLoad;*/
/**/
/*%do i=1 %to &Numfiles.;*/
/**/
/*%let i = 4;*/
/**/
/*proc sql;*/
/*	select table into :TableToLoad*/
/*	from tables*/
/*	row_number (4);*/
/*quit;*/
/*%put &TableToLoad;*/
/**/
/*%LoadTable(&TableToLoad);*/
/**/
/*%end;*/
/**/
/*%mend;*/
/**/
/*%ActualLoad;*/


